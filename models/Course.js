const mongoose = require('mongoose');

// database 
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	 	},
	description:{
		type: String,
		required: [true, "Course is required"]
		},
	price:{
		type: Number,
		required: [true, "Price is required"]
		},
	slots:{
		type: Number,
		required: [true, "Slot is required"]
		},
	isActive:{
		type: Boolean,
		default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
			// Instantiate the current date
		},
		enrollees: [
		{
			userID: {
				type: String,
				required: [true, "UserId is required"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}
		]

		
});

module.exports = mongoose.model("Course", courseSchema);

// Activity 

// Create a "User.js" file to store the schema of our users
// models > User.js 
