const express = require("express");
// import a router
const router = express.Router()
const auth = require("../auth")

const userController = require("../controllers/userControllers")
	// Route for checking email
	router.post("/checkEmail", userController.checkEmailExists);
	// Route for registration
	router.post("/register", userController.checkEmailExists,userController.registerUser);
	// Router for log in
	router.post("/login", userController.loginUser);

	router.post("/details",auth.verify, userController.returnNoPassword)

	router.get("/profile", userController.profileDetails)

	router.patch("/updateRole/:userId", auth.verify, userController.updateRole)
	// enrollment
	router.post("/enroll/:courseId", auth.verify, userController.enroll) 
module.exports = router;