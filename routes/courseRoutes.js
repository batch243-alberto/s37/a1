const express = require('express');
const router = express.Router();
const auth = require("../auth")

const courseControllers = require('../controllers/courseControllers');

router.post("/", auth.verify, courseControllers.addCourse);

router.get("/allCourses", auth.verify, courseControllers.getAllCourses);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive)

// Specific course, ":/" url = .params instead of .body
// reques.params is in /:courseId
router.get("/:courseId", courseControllers.getCourse);


// update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourses);



module.exports = router;