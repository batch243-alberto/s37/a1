const User = require("../models/User");
const Course = require("../models/Course")
// creation of users 
const bcrypt = require("bcrypt");
// Check if email already exist 
const auth = require("../auth")
/*
	Steps:
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response to the frontend application based on the result of the find method
*/
// nirereturn ni find ung array 
// result.length greater than 0 means there is a duplicate
module.exports.checkEmailExists = (request, response, next) =>{
	return User.find({email:request.body.email}).then(result =>{

		console.log(request.body.email)
		let message = ``;
		// find method returns an array record of mathching documents
		if(result.length > 0){
			message = `The ${request.body.email} is already taken, please use other email.`
			return response.send(message);
		}
		// No duplicate email found
		else{
			/*message = `That the email ${request.body.email} is not yet taken.`
			return response.send(message);*/
			return next();
		}
	})
}

module.exports.registerUser = (request, response) =>{
	// Creates a variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request.body to gather necessary information
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		// salt = salt rounds that the bcrypt algorithm will run to encrypt the password
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// Saves the created object to our database
	// .save() will not overwrite put document to the collection User
	return newUser.save().then(user => {
		console.log(user);
		response.send(`Congratutions, Sir/Ma'am ${newUser.firstName}! You are now registerd.`)
	}).catch(error =>{
		console.log(error);
		response.send(`Sorry ${newUSer.firstName} there wasn an error during the registration. Please try again!`)
	})
}
// User Authentication 
/*
	Steps:
		1. Check database if the user email exist.
		2. Compare the password provided in the login form with the password stored in the database.
*/
module.exports.loginUser = (request, response) =>{
	// the findOne methpd, returns the first record in the collection that matches the search criteria.

	return User.findOne({email: request.body.email}).then(result =>{
		if(result === null){
			response.send(`Your email: ${request.body.email} is not yet registered. Register first!`)
		}
		else{
			// return a value of true if the password is equal 
			// Creates the variable "isPasswordCorret" to return the result of the comparing the login form password and the database password
			// The compareSync method is used to compare a none encrypted password from the login form to the encrypted password retrieve. It will return true or false value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
				if(isPasswordCorrect) /*=== true*/{
					let token =  auth.createAccessToken(result)
					console.log(token)
					return response.send({accessToken: token});
				}
				else{
					return response.send(`Incorrect password, please try again!`);
				}
		}
	})
}

module.exports.returnNoPassword = (request,response) =>{
	return User.findOne({_id: request.body.id}).then(result =>{
		if(result === null){
			return response.send (`Your id ${request.body.id} is not found`)
		}
		else {
			result.password = "";
			return response.send(result)
		}
	}).catch(error => {
		console.log(error);
		return response.send(error);
	})
}

module.exports.profileDetails = (request, response) =>{
	// user will be object that contains the id and email of the user currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData)

	return User.findById(userData.id).then(result => {
		result.password = "Confidential";
		return response.send(result);
	}).catch(err =>{
		return response.send(err);
	})
}

/*module.exports.getProfile = (request, response) =>{

	return User.findById(request.body.id).then(result => {
		result.password = "******";
		console.log(result);
		return response.send(result);
	}).catch(error => {
		console.log(error);
		return response.send(error);
	})
}*/

// Update role 
module.exports.updateRole = (request, response) =>{
	const token = request.headers.authorization
	const userData = auth.decode(token);

	let idToBeUpdated = request.params.userId; 

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result =>{
			let update = {
				isAdmin: !result.isAdmin
			}

			return User.findByIdAndUpdate(idToBeUpdated,update, {new: true}).then(document =>
				response.send(document))	
		}).catch(err => response.send(err))
			}
	else{
		return response.send("You don't have access on this page!")
	}
}

// Enrollment 

module.exports.enroll = async (request, response) =>{
	const token = request.headers.authorization
	const userData = auth.decode(token);

	if(!userData.isAdmin){

		let data ={
			courseId: request.params.courseId,
			userId: userData.id
		}	
		let isCourseUpdated = await Course.findById(data.courseId).then(result => {
			result.enrollees.push({
				userID: data.userId
			})
			result.slots -= 1;

			return result.save().then(success =>{
				return true;
			}).catch(err => {console.log(err) 
				return false})
		}).catch(err => {console.log(err)
			return response.send(false);
		})

		let isUserUpdated = await User.findById(data.userId).then(result =>{
			result.enrollments.push({
				courseId: data.courseId
			})
			return result.save().then(success =>{
				return true;
			}).catch(err => false)
		}).catch(err => response.send(false))

		console.log(isUserUpdated)
		console.log(isCourseUpdated)

		return (isUserUpdated && isCourseUpdated) ? response.send("You are now enrolled!") : response.send("We encountered an error in your enrollment, please try again!")
	}
	else{
		return response.send("You are an admin, you cannot enroll a course")
	}
}