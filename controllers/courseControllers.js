const Course = require("../models/Course");
const auth = require("../auth")

/* Business Logic
	Steps:
	1. Create a new Course object using mongoose model and information from request of the user 
	2. Save the new course to the database. 
*/
module.exports.addCourse = (request,response) =>{

	let newCourse = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
		})

	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
	newCourse.save().then(result =>{
		console.log(result);
		return response.send(true);
	}).catch(error =>{
		console.log(error);
		response.send(false);
	})
	}
	else{
		return response.send("Your are not an admin");
	}
}

// Retrieve all active courses 

// isAllActive default :true
module.exports.getAllActive = (request, response) =>{
	return Course.find({isActive: true}).then(result =>{
		response.send(result)
	}).catch(err =>{
		response.send(err);
	})
}

// retrieving a specific course

module.exports.getCourse = (request, response) =>{
	const courseId = request.params.courseId;
	return Course.findById(courseId).then(result =>{
		response.send(result)
	}).catch(err =>{
		response.send(err)
	})
}

// update a course 

module.exports.updateCourse = (request,response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

		let updatedCourse = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		}
	const courseId = request.params.courseId;
	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new: true}).then(result =>{ 
			response.send(result)
			// new: true for update to be save and return the updated course
		}).catch(err =>{
			response.send(err);
		})
	} 
	else{
		return response.send("You don't have access to this page!");
	}
}

module.exports.archiveCourses = (request, response) =>{
	const userData = auth.decode(request.headers.authorization)
	console.log(userData); 

	let inactiveCourses ={
		isActive: request.body.isActive
	}
	const courseId = request.params.courseId;
	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, inactiveCourses, {new: true}).then(result =>{
			response.send(true)
		}).catch(err =>{
			response.send(err);
		})
	}
	else{
		response.send("You are not authorized");
	}
}

// Retrieve all courses including those inactive course

/*
	Steps:
		1. Retrieve all the courses(active/inactive) from the database.
		2. Verify the role of the current user(Admin to continue)
*/
module.exports.getAllCourses = (request, response) =>{
	const token = request.headers.authorization
	const userData = auth.decode(token);

	if(!userData.isAdmin){
		return response.send("Sorry, you don't have access to this page!")
	}
	else{
		return Course.find({}).then(result => response.send(result)) 
		// Automatic return so need to {}
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	}
}

