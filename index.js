// setup dependencies

const express = require('express');
const mongoose = require('mongoose');
// get response form user, our API gives a result to the user
const cors = require('cors');

// import, allow access to routes defined within our application 
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")
const app = express();

// Allows all resources to access our backend application,middlewares
app.use(cors());
app.use(express());
// express,json()function is a built-in middleware function in express. It parses incoming request with JSON payloads.
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Defines the "/users" be to be inluded for all user routes defined in the userRoutes route file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
// Database connection 
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243-alberto.j1er3yl.mongodb.net/APIBooking?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
// Catch an error, connection error is the message that will display of an error is encountered
mongoose.connection.on("error", console.error.bind(console, "connection error"));
// Prompt the message of the terminal if successfully connected 
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"));

// process.env.PORT can be assigned by your hosting service
app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})

